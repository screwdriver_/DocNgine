#include "doc_ngine.hpp"

#include<dirent.h>

using namespace DocNgine;

Document Database::getDocument(const string& path) const
{
	return Document(directory, path);
}

vector<Document> list(const string& db_directory,
	const string& path, const bool recursive)
{
	DIR* dir;
	dirent* entry;

	makeDirPath(path);

	if((dir = opendir(path.c_str())) == nullptr) {
		throw runtime_error(string("Can't open folder `")
			+ path + "`!");
	}

	vector<Document> documents;

	while((entry = readdir(dir)) != nullptr) {
		const string name = entry->d_name;

		if(entry->d_type == DT_DIR && recursive) {
			const string p = path + name;
			const auto docs = list(db_directory, p, true);

			for(const auto& d : docs) {
				documents.push_back(d);
			}
		} else {
			documents.emplace_back(db_directory, name);
		}
	}

	closedir(dir);

	return documents;
}

vector<Document> Database::listDocuments(const string& path,
	const bool recursive) const
{
	const string p = directory + DOCUMENTS_DIR + path;
	return list(directory, p, recursive);
}
